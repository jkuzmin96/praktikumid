package Praktikum4;

import lib.TextIO;

public class Tsyklid_jagatud_3 {

	public static void main(String[] args) {
		
        System.out.println("Palun sisesta tabeli suurus");
		int tabeliSuurus = TextIO.getlnInt();
        
        for (int i = 0; i < tabeliSuurus * 2 - 1; i++) {
        	System.out.print("-");
        }
        System.out.println();
        
        for (int j = 0; j < tabeliSuurus; j++) {
        	System.out.print("| ");
            for (int i = 0; i < tabeliSuurus; i++) {
                if (i == j || i + j == tabeliSuurus - 1)
                    System.out.print("x ");
                else 
                    System.out.print("0 ");
                
                //System.out.print("(j=" + j + "i=" + i + ")");
            }
            System.out.print("| ");
            System.out.println(); // reavahetus
        }
        for (int i = 0; i < tabeliSuurus * 2 - 1; i++) {
        	System.out.print("-");
        }
    }
}