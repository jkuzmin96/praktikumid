package Praktikum6;

import lib.TextIO;
import praktikum5.Meetodid;

public class KullJaKiri {

    public static void main(String[] args) {
        
        int kasutajaRaha = 10;
        
        while (kasutajaRaha > 0) {
            System.out.println("Sul on " + kasutajaRaha + " raha.");
            int maxPanus =Math.min(kasutajaRaha, 25);
            System.out.println("Palun sisesta panus (max" + maxPanus +") ;"
            int panus = Meetodid.kasutajaSisestus(1, maxPanus);
            kasutajaRaha -= panus;
            
            int kullV6iKiri = ArvamusM2ng.suvalineArv(0, 1);
            
            if (kullV6iKiri == 0) { // Kull
                System.out.println("VÕITSID, saad topelt raha tagasi!");
                kasutajaRaha += panus * 2;
            } else { // Kiri
                System.out.println("Kaotasid...");
            }
            
        }
        
    }

}
