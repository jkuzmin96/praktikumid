package Praktikum2;

import lib.TextIO;

public class GruppideArv {
	
	public static void main(String[] args) {
		
		//sysout -> Ctrl + tühik
		System.out.println("Palun sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();
		
		System.out.println("Palun sisesta grupi suurus");
		int grupiSuurus = TextIO.getlnInt();
		
		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Gruppide arv on: " + gruppideArv);
		
		
		int j22k = inimesteArv % grupiSuurus;
		System.out.println("Üle jääb " + j22k + " inimest.");
	}
}
