package Praktikum7;

public class KymmeArvu {

	public static void main(String[] args) {
	
		int arv; // muutuja deklaratsioon
		int[] arvud = new int[10]; // massivi deklaratsioon
		
		
		arv = 7; // muutuja väärtustamine
		arvud[0] = 7; // massiivi ühe elemendi väärtustamine
		arvud[1] = 8;
		
		// System.out.println(arv); // väljastamine
		
		for (int i = 0; i < arvud.length; i++) {
			System.out.println(arvud[i]);
		}
		System.out.println(arvud);
		tryki(arvud);
		
		
	}

	public static void tryki (int[] massiiv) {
		System.out.print("[");
		for (int i = 0; i < massiiv.length; i++) {
			if (i > 0)
				System.out.print(", ");
			System.out.print(massiiv[i]);
		}
		System.out.print("]");
		}
	}
